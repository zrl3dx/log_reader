# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'log_reader/version'

Gem::Specification.new do |spec|
  spec.name          = 'log_reader'
  spec.version       = LogReader::VERSION
  spec.authors       = ['Dominik Halat']
  spec.email         = ['dominik.halat@gmail.com']

  spec.summary       = 'Log Reader for well... log reading and parsing'
  spec.metadata['allowed_push_host'] = 'https://gitlab.com/'
  spec.metadata['source_code_uri'] = 'https://gitlab.com/gitlab-org/security-products/license-management'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/gitlab-org/security-products/license-management/blob/master/CHANGELOG.md'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(bin|test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
  spec.required_ruby_version = '~> 2.7.1'

  spec.add_dependency 'dotenv'
  spec.add_dependency 'dry-struct', '~> 1.2'
  spec.add_dependency 'dry-types', '~> 1.2'
  spec.add_dependency 'dry-validation', '~> 1.2'
  spec.add_dependency 'faraday', '~> 1.0.1'
  spec.add_dependency 'oj'
  spec.add_dependency 'ox'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'ci_reporter_rspec'
  spec.add_development_dependency 'dotenv'
  spec.add_development_dependency 'dry-validation-matchers'
  spec.add_development_dependency 'factory_bot'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec-dry-struct'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-checkstyle_formatter'
  spec.add_development_dependency 'rubocop-rspec'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'webmock'
end
