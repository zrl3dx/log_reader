# Log Reader

Displaying sample feeds:


        bin/console
        LogReader.test_ftp
        LogReader.test_xml
        LogReader.test_varnish(stream: true)
        LogReader.test_varnish(stream: false)





Simple parsing speed comparison: 

require 'benchmark'

        Benchmark.bm do |bm|
          bm.report('oj_call') { 5000.times { parser.oj_call(data) } }
          bm.report('saj_call') { 5000.times { parser.call(data) } }
        end
