# frozen_string_literal: true

module Faraday
  class FtpConnection < ::Faraday::Connection
    def url_prefix=(url, _encoder = nil)
      uri = @url_prefix = Utils.URI(url)
      self.path_prefix = uri.path
    end

    def path_prefix=(value)
      url_prefix.path = value
    end
  end
end
