# frozen_string_literal: true

class FtpAdapter < ::Faraday::Adapter
  dependency 'net/ftp'

  register_middleware(File.expand_path(__dir__), net_ftp: [:FtpAdapter, 'ftp_adapter'])

  DEFAULT_BLOCKSIZE = 1024 # in theory those are lines so NDJSON would be great but in reality those are only chars

  def build_connection(env)
    Net::FTP.new(env[:url].host, @connection_options)
  end

  def call(env)
    body = ''
    result = connection(env).tap do |c|
      c.binary = @connection_options[:binary]
      handle_auth(env, c)

      c.get(env[:url].path, nil, DEFAULT_BLOCKSIZE) do |chunk|
        body += chunk
      end
    end

    super
    save_response(env, result.last_response_code, body, nil, result.last_response)
  end

  private

  def handle_auth(env, connection)
    connection.login(env[:url].user || 'anonymous', env[:url].password)
  end
end
