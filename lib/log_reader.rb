# frozen_string_literal: true

require 'oj'
require 'ox'
require 'faraday'
require 'net/ftp'
require 'pry'
require 'dry-types'
require 'dry-struct'
require 'dry-validation'

Dir[File.join(__dir__, '**/*.rb')].sort.each { |file| require file }

module LogReader
  class << self
    def test_ftp
      puts LogReader::FeedHandler.new(fetcher: LogReader::Fetcher::FTP.new(url: ENV.fetch('FTP_FEED_ENDPOINT'),
                                                                           client: Faraday::FtpConnection),
                                      parser: LogReader::Parser::JSON.new(fields: %i[price title full_url]),
                                      processor: LogReader::Processor::Product.new(mapping: { price: 'price',
                                                                                              name: 'title',
                                                                                              url: 'full_url' }),
                                      presenter: LogReader::Presenter::Product.new).call.join("\n")
    end

    def test_xml
      puts LogReader::FeedHandler.new(fetcher: LogReader::Fetcher::XML.new(url: ENV.fetch('XML_FEED_ENDPOINT')),
                                      parser: LogReader::Parser::XML.new(fields: %i[g:price title link],
                                                                         uniq_id: :'g:id', price_field: :'g:price'),
                                      processor: LogReader::Processor::Product.new(mapping: { price: :'g:price',
                                                                                              name: :title,
                                                                                              url: :link }),
                                      presenter: LogReader::Presenter::Product.new).call.join("\n")
    end

    def test_varnish(stream: true)
      puts LogReader::FeedHandler.new(fetcher: LogReader::Fetcher::File.new(url: ENV.fetch('VARNISH_FILE_ENDPOINT'),
                                                                            client: File),
                                      parser: LogReader::Parser::Varnish.new,
                                      processor: LogReader::Processor::Varnish.new,
                                      presenter: LogReader::Presenter::LogEntry.new(limit: 5)).call(stream: stream)
                                 .join("\n")
    end
  end
end
