# frozen_string_literal: true

require_relative 'types'

module LogReader
  class Product < Dry::Struct
    attribute :price, Types::Coercible::Decimal
    attribute :name, Types::Strict::String
    attribute :url, Types::Strict::String
  end
end
