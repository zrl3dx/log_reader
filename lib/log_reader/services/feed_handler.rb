# frozen_string_literal: true

module LogReader
  class FeedHandler
    def initialize(fetcher:, parser:, processor:, presenter:)
      @fetcher = fetcher
      @parser = parser
      @processor = processor
      @presenter = presenter
    end

    def call(stream: false)
      stream ? fetch_feed { |blk| @parse = parse_feed(blk) } : fetch_feed.each_line { |line| @parse = parse_feed(line) }
      processed_feed = process_feed(@parse)
      present_feed(processed_feed)
    end

    private

    attr_reader :fetcher, :parser, :processor, :presenter

    def fetch_feed(&blk)
      fetcher.call(&blk)
    end

    def parse_feed(feed)
      parser.call(feed)
    end

    def process_feed(feed)
      processor.call(feed)
    end

    def present_feed(feed)
      presenter.call(feed)
    end
  end
end
