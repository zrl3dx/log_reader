# frozen_string_literal: true

module LogReader
  module Processor
    class Varnish
      def call(data)
        data
      end
    end
  end
end
