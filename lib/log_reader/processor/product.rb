# frozen_string_literal: true

module LogReader
  module Processor
    class Product
      def initialize(mapping:)
        @mapping = mapping
      end

      def call(products_feed)
        to_product(filter_invalid(process_params(products_feed)))
      end

      private

      attr_reader :mapping

      def to_product(json)
        json.map { |params| LogReader::Product.new(params) }
      end

      def filter_invalid(products)
        products.select { |payload| LogReader::Contracts::Product.new.call(payload).errors.empty? }
      end

      def process_params(feed)
        feed.map do |payload|
          { name: payload[mapping[:name]],
            price: payload[mapping[:price]],
            url: payload[mapping[:url]] }
        end
      end
    end
  end
end
