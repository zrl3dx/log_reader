# frozen_string_literal: true

module LogReader
  module Fetcher
    class XML < Base
      def call
        client.new(url).get.body
      end
    end
  end
end
