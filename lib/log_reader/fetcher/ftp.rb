# frozen_string_literal: true

require_relative '../../faraday/adapters/ftp_adapter'

module LogReader
  module Fetcher
    class FTP < Base
      def call
        client.new(url) do |conn|
          conn.adapter 'FtpAdapter', { binary: false }
        end.get.body
      end
    end
  end
end
