# frozen_string_literal: true

module LogReader
  module Fetcher
    class File < Base
      def call(&block)
        if block_given?
          client.open(url, 'r').each_line do |line|
            block.call(line)
          end
        else
          client.open(url, 'r').read
        end
      end
    end
  end
end
