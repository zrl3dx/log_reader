# frozen_string_literal: true

module LogReader
  module Fetcher
    class Base
      def initialize(url:, client: Faraday::Connection)
        @url = url
        @client = client
      end

      def call
        raise NotImplementedError
      end

      private

      attr_reader :url, :client
    end
  end
end
