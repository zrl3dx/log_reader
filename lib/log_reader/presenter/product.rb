# frozen_string_literal: true

module LogReader
  module Presenter
    class Product
      def call(products, sort_key: :price)
        @products = products
        sort_by(sort_key).map { |product| to_display(product) }
      end

      private

      attr_reader :products

      def sort_by(atr)
        products.sort { |p1, p2| p2.public_send(atr) <=> p1.public_send(atr) }
      end

      # where is currency?
      def to_display(product)
        "#{format(product.name, :name)} #{format(product.price.to_f, :price)} #{format(product.url, :url)}"
      end

      def format(string, key)
        string.to_s.ljust(max_by(key) + 2, ' ')
      end

      def max_by(key)
        products.map { |p| p.public_send(key).to_s }.max_by(&:size).length
      end
    end
  end
end
