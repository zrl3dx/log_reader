# frozen_string_literal: true

module LogReader
  module Presenter
    class LogEntry
      def initialize(limit:)
        @limit = limit
      end

      def call(entries)
        @entries = entries
        [to_display(sort(entries[:hosts]), :hosts), to_display(sort(entries[:paths]), :paths)]
      end

      private

      attr_reader :limit, :entries

      def sort(entries)
        entries.invert.compact.sort.reverse.first(limit)
      end

      # where is currency?
      def to_display(chunk, label)
        chunk.map do |(count, name)|
          "#{label}: #{format(name, label)}  hits: #{format(count, label)}"
        end
      end

      def format(string, key)
        string.to_s.ljust(max_by(key) + 2, ' ')
      end

      def max_by(key)
        entries[key].keys.compact.max_by(&:length).length
      end
    end
  end
end
