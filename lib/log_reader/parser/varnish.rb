# frozen_string_literal: true

module LogReader
  module Parser
    class Varnish
      def initialize
        @result = { hosts: Hash.new(0), paths: Hash.new(0) }
      end

      def call(data)
        uri = parse_line(data)
        host = uri.host
        path = uri.path.split('/').last

        result[:hosts][host] += 1
        result[:paths][path] += 1
        result
      end

      attr_reader :result

      private

      # this should be done better - fetch status, etc.
      def parse_line(line)
        URI.parse(URI.extract(line)[0])
      end
    end
  end
end
