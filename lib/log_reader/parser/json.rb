# frozen_string_literal: true

module LogReader
  module Parser
    class JSON < ::Oj::Saj
      def initialize(fields: [])
        super()
        @fields = fields.map(&:to_s)

        @hash_cnt = 0
        @result = []
      end

      def hash_start(_key)
        @hash_cnt += 1
      end

      def add_value(value, key)
        return unless fields.include?(key)

        @result[@hash_cnt - 1] ||= {}
        @result[@hash_cnt - 1][key] = value
      end

      def error(message, line, column)
        "ERRROR: #{line}:#{column}: #{message}"
      end

      def call(data)
        handler = self
        Oj.saj_parse(self, data)
        reset!
        handler.result
      end

      # just for benchmarking, might also compare ScHandler which should be ~20% faster than regular Oj.load
      def oj_call(data)
        Oj.load(data)
      end

      attr_reader :result

      private

      def reset!
        @hash_cnt = 0
      end

      attr_reader :data, :fields
    end
  end
end
