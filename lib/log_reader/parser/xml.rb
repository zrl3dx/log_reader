# frozen_string_literal: true

module LogReader
  module Parser
    class XML < ::Ox::Sax
      def initialize(fields: [], uniq_id:, price_field:)
        super()
        @fields = fields
        @uniq_id_field = uniq_id
        @price_field = price_field
        @result = {}
      end

      def start_element(name)
        @current_attribute = name
      end

      def end_element(_name)
        @current_attribute = nil
      end

      def text(value)
        @uniq_id_value = value if @uniq_id_field == @current_attribute

        return unless fields.include?(@current_attribute)
        return unless @uniq_id_value

        @result[@uniq_id_value] ||= {}
        @result[@uniq_id_value][@current_attribute] = extract_price(@current_attribute, value)
      end

      def call(data)
        handler = self
        Ox.sax_parse(self, data)
        reset!
        handler.result.values
      end

      # just for benchmarking, might also compare ScHandler which should be ~20% faster than regular Oj.load
      def ox_call(data)
        Ox.load(data)
      end

      attr_reader :result

      private

      def reset!
        @hash_cnt = 0
      end

      attr_reader :data, :price_field, :fields

      def extract_price(field, value)
        return value unless field == price_field

        value.scan(/\d+.\d+/).first
      end
    end
  end
end
