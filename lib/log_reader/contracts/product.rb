# frozen_string_literal: true

require 'uri'

module LogReader
  module Contracts
    class Product < Dry::Validation::Contract
      params do
        required(:price).filled(:decimal)
        required(:name).filled(:string)
        required(:url).filled(:string)
      end

      rule(:url) do
        key.failure('invalid format') unless value =~ URI::DEFAULT_PARSER.make_regexp # addressable is better
      end
    end
  end
end
