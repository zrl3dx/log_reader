# frozen_string_literal: true

require 'spec_helper'

describe LogReader::Fetcher::XML do
  subject(:ftp) { described_class.new(url: url, client: Faraday::Connection) }
  let(:url) { 'http://asd.asd.asd' }
  let(:client) { instance_double(Faraday::Connection) }

  describe '#call' do
    before do
      expect(Faraday::Connection).to receive(:new).with(url) { client }
      allow(client).to receive(:get) { OpenStruct.new(body: '42') }
    end

    it 'makes proper request' do
      expect(client).to receive(:get)

      subject.call
    end
  end
end
