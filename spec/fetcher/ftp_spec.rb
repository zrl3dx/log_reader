# frozen_string_literal: true

require 'spec_helper'

describe LogReader::Fetcher::FTP do
  subject(:ftp) { described_class.new(url: url, client: Faraday::FtpConnection) }
  let(:url) { 'ftp://some.ftp' }
  let(:client) { instance_double(Faraday::FtpConnection) }

  describe '#call' do
    before do
      expect(Faraday::FtpConnection).to receive(:new).with(url) { client }
      allow(client).to receive(:get) { OpenStruct.new(body: '42') }
    end

    it 'makes proper request' do
      expect(client).to receive(:get)

      subject.call
    end
  end
end
