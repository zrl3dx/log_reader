# frozen_string_literal: true

require 'spec_helper'

describe LogReader::Presenter::Product do
  subject(:present) { described_class.new.call(products, sort_key: sort_key) }
  let(:products) { FactoryBot.create_list(:product, 3) }
  let(:sort_key) { :price }
  describe '#call' do
    it 'does not lose Products' do
      expect(present.size).to eq products.size
    end

    it 'sorts by price' do
      expect(present[0]).to include(products.max_by(&:price).price.to_f.to_s)
    end
  end
end
