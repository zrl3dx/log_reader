# frozen_string_literal: true

require 'spec_helper'

describe LogReader::Contracts::Product, type: %i[dry_validation] do
  it { is_expected.to validate(:price, :required).filled(:decimal) }
  it { is_expected.to validate(:name, :required).filled }
  it { is_expected.to validate(:url, :required).filled }
end
