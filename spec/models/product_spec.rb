# frozen_string_literal: true

require 'spec_helper'

describe LogReader::Product do
  describe 'attributes' do
    subject { described_class }
    it { is_expected.to have_attribute(:price, Types::Coercible::Decimal) }
    it { is_expected.to have_attribute(:name, Types::Strict::String) }
    it { is_expected.to have_attribute(:url, Types::Strict::String) }
  end
end
