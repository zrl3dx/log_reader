# frozen_string_literal: true

require 'spec_helper'

describe LogReader::FeedHandler do
  subject(:process) do
    described_class.new(fetcher: fetcher, parser: parser,
                        processor: processor, presenter: presenter).call
  end
  let(:fetcher) { instance_double(LogReader::Fetcher::FTP) }
  let(:parser) { instance_double(LogReader::Parser::JSON) }
  let(:processor) { instance_double(LogReader::Processor::Product) }
  let(:presenter) { instance_double(LogReader::Presenter::Product) }
  let(:feed) { anything }

  describe '#call' do
    before do
      allow(feed).to receive(:each_line) {}
      expect(fetcher).to receive(:call) { 'asd' }
      expect(parser).to receive(:call) { feed }
      expect(processor).to receive(:call).with(feed) { feed }
      expect(presenter).to receive(:call).with(feed) { feed }
    end

    it 'calls proper services' do
      process
    end
  end
end
