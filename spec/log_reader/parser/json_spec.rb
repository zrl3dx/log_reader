# frozen_string_literal: true

require 'spec_helper'

describe LogReader::Parser::JSON do
  subject(:parser) { described_class.new(fields: fields) }
  let(:fields) { %i[price title full_url] }

  describe '#call' do
    context 'with valid data' do
      let(:data) { FactoryBot.build(:json_data, kind: :valid) }

      it 'parses valid data' do
        expect(parser.call(data).size).to eq(24)
      end

      it 'parses only specified fields' do
        expect(parser.call(data).first.keys).to match_array(fields.map(&:to_s))
      end
    end
  end
end
