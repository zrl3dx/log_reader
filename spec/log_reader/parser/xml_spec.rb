# frozen_string_literal: true

require 'spec_helper'

describe LogReader::Parser::XML do
  subject(:parser) { described_class.new(fields: fields, uniq_id: uniq_id, price_field: price_field) }
  let(:fields) { %i[g:price title link] }
  let(:uniq_id) { :'g:id' }
  let(:price_field) { :'g:price' }

  describe '#call' do
    context 'with valid data' do
      let(:data) { FactoryBot.build(:xml_data, kind: :valid) }

      it 'parses valid data' do
        expect(parser.call(data).size).to eq(47)
      end

      it 'parses only specified fields' do
        expect(parser.call(data).first.keys).to match_array(fields)
      end
    end
  end
end
