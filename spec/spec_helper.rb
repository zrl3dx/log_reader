# frozen_string_literal: true

require 'bundler/setup'
require 'log_reader'
require 'webmock/rspec'
require 'factory_bot'
require 'dry/validation/matchers'
require 'rspec/dry/struct'

require 'pry'

FactoryBot.find_definitions

RSpec.configure do |config|
  config.example_status_persistence_file_path = 'tmp/.rspec_status'

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  WebMock.disable_net_connect!
end
