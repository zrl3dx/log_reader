# frozen_string_literal: true

FactoryBot.define do
  factory :json_data, class: Hash do
    data { File.read(File.join('spec', 'support', 'fixtures', 'json', "#{kind}.json")) }

    transient do
      kind { 'valid' }
    end

    skip_create
    initialize_with { attributes[:data] }
  end
end
