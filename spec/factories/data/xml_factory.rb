# frozen_string_literal: true

FactoryBot.define do
  factory :xml_data, class: String do
    data { File.read(File.join('spec', 'support', 'fixtures', 'xml', "#{kind}.xml")) }

    transient do
      kind { 'valid' }
    end

    skip_create
    initialize_with { attributes[:data] }
  end
end
