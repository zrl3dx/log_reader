# frozen_string_literal: true

require 'securerandom'

FactoryBot.define do
  factory :product, class: LogReader::Product do
    price { rand(1..1000) }
    sequence(:name) { |n| "Product #{n}" }
    url { "http://some.domain/#{SecureRandom.hex(10)}" }

    skip_create
    initialize_with { new(attributes) }
  end
end
