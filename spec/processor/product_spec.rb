# frozen_string_literal: true

require 'spec_helper'

describe LogReader::Processor::Product do
  subject(:process) { described_class.new(mapping: mapping).call(feed) }

  let(:mapping) { { price: 'price', name: 'title', url: 'full_url' } }
  let(:feed) do
    [{ title: 'som nam', price: '234234.32', full_url: 'invalid' },
     { title: 'The best product', price: 'NaN', full_url: 'http://very.valid' },
     { title: 'Just regular one', price: '234.52', full_url: 'http://gog.com/asd' }.stringify_keys]
  end

  describe '#call' do
    it 'filters invalid products' do
      expect(process.size).to eq 1
    end
  end
end
